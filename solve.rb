#!/usr/bin/ruby
# -*- coding: utf-8 -*-

=begin
桁数制限なしで探索すると、ものすごく時間がかかる。
消費メモリが30GBくらいになるので、相当ハイスペックなマシンじゃないと動かないので、
10ケタ前後の制限にしておいた方が無難。
省メモリ・メモ化等の効率化・高速化はしていないので、ある程度は工夫ができるはず。
=end

SizeLimit = ARGV[0].to_i # 引数なしの場合は0になる。0の場合は桁数制限なしとする
nStart, nGoal = 2014, 26
$startTime = Time.now

# パターン生成済みの文字列を記録しておく。2回目以降はここから参照する
# すべてのUnitで共有できるのでグローバルにしておく
$memo = Hash.new 

class State
  attr_accessor :generatedValue, :history
  def initialize( s )
    @generatedValue = s # 変換された後の値
    @history = Array.new # 履歴
  end
end

class Unit
  attr_accessor :stateList
  
  def initialize( startValue )
    @startValue = startValue.to_s # 最初の値。わざわざメンバ変数として保持しなくても良い気がする。
    @stateList = [ st = State.new(@startValue) ] # 新しい状態をここにどんどん突っ込んでいく。
    @generatedList = Hash.new # 生成された状態を追加していき、すでに存在するかどうかの判定に使う
    @generatedList[ @startValue ] = st # とりあえず最初の値だけ
  end
  
  def getLinkedState( state )
    # sになる平成変換があれば返す。
    @generatedList[ state.generatedValue ]
  end
  
  # 現在のすべての状態から、変換可能な状態のリストを作る
  # 前の状態は持ってても仕方ないので上書きする
  def generateNextStateList
    nextList = []
    @stateList.each{|state|
      list = generateState( state )
      nextList += list
      if $memo[ state.generatedValue ] == nil
        $memo[ state.generatedValue ] = list.dup
      end
    }
    @stateList = nextList
  end
  
  def generateState( state )
    list = []
    generateStateRec( state, "", "", 0, list )
    list
  end
  
  def generateStateRec( state, generatedValue, generatedString, startIndex, list )
    endIndex = state.generatedValue.length - 1
    
    # 文字列の終端まで来たとき
    # 新しいパターンが見つかったらリストに追加する
    if startIndex > endIndex
      if @generatedList[ generatedValue ] == nil && ( SizeLimit <= 0 || generatedValue.length < SizeLimit )
        newState = State.new( generatedValue )
        newState.history += state.history
        newState.history << generatedString
        @generatedList[ generatedValue ] = newState
        list << newState
      end
      
      return
    end
    
    # 再帰的に部分文字列を生成
    (startIndex..endIndex).each{|sliceIndex|
      str = state.generatedValue[startIndex..sliceIndex]
      generateStateRec( state, (generatedValue + str), (generatedString + str), (sliceIndex + 1), list )
      if str[0] != "0" && str.to_i != 1
        generateStateRec( state, (generatedValue + (str.to_i**2).to_s), (generatedString + "[#{str}]"), (sliceIndex + 1), list )
        n = str.to_i
        if sq = getSquare( n )
          generateStateRec( state, (generatedValue + sq.to_s), (generatedString + "(#{str})"), (sliceIndex + 1), list )
        end
      end
    }
  end
  
  # 平方数のテーブルとか作っといた方が多少速いとは思う
  def getSquare(n)
    m = Math.sqrt(n).to_i
    m*m == n ? m : false
  end

end

# goal側から探索した場合は、出力するのに文字列（式）の逆変換が必要
def getInverse( history )
  history.map{|s|
    s.gsub(/[\[(]\d+[)\]]/){|item|
      /\d+/ =~ item
      n = $&.to_i
      if item[0] == "["
        "(#{n**2})"
      else
        "[#{Math.sqrt(n).to_i}]"
      end
    }
  }.reverse
end

# 双方向から繋がりがあるかを調べる
# 見つかった解はすべてリストに追加しておくことにする
def getSolutionList( uStart, uGoal )
  
  solution = []
  
  uStart.stateList.each{|state1|
    state2 = uGoal.getLinkedState( state1 )
    if state2 != nil
      solution << state1.history + getInverse( state2.history )
    end
  }
  
  solution
end

# 1ステップ進める
def nextStep( side, unit )
    print"Side-#{side} (#{unit.stateList.size} => "
    unit.generateNextStateList()
    puts"#{unit.stateList.size} items) #{getTime()}"
end

def solve( nStart, nGoal )
  uStart = Unit.new( nStart )
  uGoal = Unit.new( nGoal )
  
  stepCount = 0
  loop{
    print"step #{ stepCount += 1 }: "
    
    # 現在の状態数が少ない方から展開していく
    if uStart.stateList.size < uGoal.stateList.size
      nextStep( 'S', uStart )
    else
      nextStep( 'G', uGoal )
    end
    
    solution = getSolutionList( uStart, uGoal )
    
    if solution.size > 0
      solution = solution.map{|sol|
        ( sol + [nGoal] ).join("->")
      }.sort{|a,b|a.length<=>b.length}
      solution.map{|s| puts s }
      puts"Total Solution: #{ solution.size }"
      puts"shortest(#{solution[0].length})B: #{solution[0]}"
      return
    end
    
    # 新しい状態が作れなくなったら解なし
    if uStart.stateList.empty? || uGoal.stateList.empty?
      puts"Solution not found."
      return
    end
  }
end

def getTime
  "%dms"%[ ( (Time.now - $startTime) * 1000 ).to_i ]
end

puts"Size Limit: #{SizeLimit > 0 ? SizeLimit : 'None' }"
solve( nStart, nGoal )
puts"Finish Time: #{getTime()}"

